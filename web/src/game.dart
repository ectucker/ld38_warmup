// Copyright (c) 2017, Ethan Tucker. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

part of game;

class JuicyBreakout extends BaseGame {

  SpriteBatch batch;
  Framebuffer fbo;

  Camera2D camera;
  Camera2D fboCamera;

  Map<String, Texture> atlas;

  double time = 0.0;

  List<Brick> bricks;
  Paddle paddle;
  Ball ball;

  List<StreamSubscription> subscriptions = [];
  bool left, right;

  bool moving = false;

  Sound bounceSound, breakSound, endSound;
  bool endPlayed = false;

  @override
  create() {
    setGLViewport(width, height);

    batch = new SpriteBatch.defaultShader();
    fbo = new Framebuffer(canvasWidth, canvasHeight, shader: assetManager.get("post"));
    camera = new Camera2D.originBottomLeft(width, height);
    camera.update();
    fboCamera = new Camera2D.originCenter(width, height);
    fboCamera.update();

    atlas = assetManager.get("sprites");

    bounceSound = assetManager.get("bounce.wav");
    breakSound = assetManager.get("break.wav");
    endSound = assetManager.get("end.wav");

    paddle = createPaddle();
    ball = createBall();
    bricks = generateBricks();

    resume();
  }

  @override
  preload() {
    assetManager.load("post", loadProgram("assets/shaders/bounce.vertex", "assets/shaders/average.fragment"));
    assetManager.load("sprites", loadAtlas("assets/sprites.json", loadTexture("assets/sprites.png")));

    assetManager.load("bounce.wav", loadSound("assets/sound/bounce.wav"));
    assetManager.load("break.wav", loadSound("assets/sound/break.wav"));
    assetManager.load("end.wav", loadSound("assets/sound/end.wav"));
  }

  @override
  render(num delta) {
    clearScreen(0.0, 0.0, 0.0, 1.0);
    fbo.beginCapture();
    clearScreen(0.0, 0.0, 0.0, 1.0);

    batch.projection = camera.combined;
    batch.begin();
    paddle.render(batch);
    bricks.forEach((Brick b) => b.render(batch));
    ball.render(batch);
    batch.end();

    fbo.endCapture();
    fbo.shader.startProgram();
    fbo.setUniform("time", new Vector2(time, 0.0));
    fbo.setUniform("uTextureSize", new Vector2(fbo.width.toDouble(), fbo.height.toDouble()));
    fbo.render(fboCamera.combined, -width ~/ 2, -height ~/ 2, width, height);
  }

  @override
  update(num delta) {
    if(moving) {
      if (left) {
        paddle.move(-delta * 150);
      }
      if (right) {
        paddle.move(delta * 150);
      }
      ball.bounce(paddle.hitbox);
      if(paddle.hitbox.intersectsWithAabb2(ball.hitbox)) {
        bounceSound.play();
      }
      bricks.forEach((Brick b) => ball.bounce(b.hitbox));
      List<Brick> toRemove = [];
      for (Brick b in bricks) {
        if (b.hitbox.intersectsWithAabb2(ball.hitbox)) {
          toRemove.add(b);
        }
      }
      if(toRemove.length > 0) {
        breakSound.play();
      }
      toRemove.forEach((Brick b) => bricks.remove(b));
      toRemove.clear();
      if (ball.x > width - ball.width) {
        ball.velocityX = -ball.velocityX;
        bounceSound.play();
      }
      if (ball.x < 0) {
        ball.velocityX = -ball.velocityX;
        bounceSound.play();
      }
      if (ball.y > height - ball.height) {
        ball.velocityY = -ball.velocityY;
        bounceSound.play();
      }
      ball.update(delta);
      if (ball.y < -ball.height) {
        if(!endPlayed) {
          endSound.play();
          endPlayed = true;
        }
        time += delta * 20;
        if (time >= 5 * PI) {
          time = 0.0;
          ball = createBall();
          paddle = createPaddle();
          moving = false;
          endPlayed = false;
        }
      }
    }
  }

  @override
  resize(num width, num height) {
    setGLViewport(width, height);
  }

  List<Brick> generateBricks() {
    List<Brick> bricks = [];
    List<String> colors = ["red", "yellow", "green", "ltblue", "blue"];
    double brickWidth = width / 10;
    double brickHeight = brickWidth / 3;
    for(int y = 0; y < colors.length; y++) {
      for(int x = 0; x < 10; x++) {
        bricks.add(new Brick(atlas[colors[y]],
            x * brickWidth, height - y * brickHeight - brickHeight * 3,
            brickWidth, brickHeight));
      }
    }
    return bricks;
  }

  Paddle createPaddle() {
    return new Paddle(atlas["white"], width / 2 - (width / 7 / 2), height / 30, width / 7, height / 25);
  }

  Ball createBall() {
    return new Ball(atlas["white"], width / 2 - width / 80, height / 12, width / 40, width / 40);
  }

  void keyDown(KeyboardEvent e) {
    if(e.keyCode == KeyCode.LEFT) {
      left = true;
      if(!moving) {
        moving = true;
        ball.velocityX = -1.0;
      }
    }
    if(e.keyCode == KeyCode.RIGHT) {
      right = true;
      if(!moving) {
        moving = true;
        ball.velocityX = 1.0;
      }
    }
  }

  void keyUp(KeyEvent e) {
    if(e.keyCode == KeyCode.LEFT) {
      left = false;
    }
    if(e.keyCode == KeyCode.RIGHT) {
      right = false;
    }
  }

  @override
  resume() {
    subscriptions.add(document.onKeyDown.listen(keyDown));
    subscriptions.add(document.onKeyUp.listen(keyUp));
  }

  @override
  pause () {
    subscriptions.forEach((StreamSubscription e) => e.cancel());
  }

}
