part of game;

class Ball {

  Texture texture;
  double x, y;
  double width, height;

  double velocityX = 1.0, velocityY = 1.0;

  Aabb2 hitbox;

  Ball(this.texture, this.x, this.y, this.width, this.height) {
    hitbox = new Aabb2.centerAndHalfExtents(new Vector2(x - width / 2 + width, y - height / 2 + height), new Vector2(width / 2, height / 2));
  }

  void move(double xDist, double yDist) {
    x += xDist;
    y += yDist;
    hitbox.setCenterAndHalfExtents(new Vector2(x - width / 2 + width, y - height / 2 + height), new Vector2(width / 2, height / 2));
  }

  void bounce(Aabb2 other) {
    if(other.intersectsWithAabb2(hitbox)) {
      if(other.max.x > hitbox.center.x && other.min.x < hitbox.center.x) {
        velocityY = -velocityY;
      } else {
        velocityX = -velocityX;
      }
    }
  }

  void update(delta) {
    move(velocityX * delta * 100, velocityY * delta * 100);
  }

  void render(SpriteBatch batch) {
    batch.draw(texture, x, y, width: width, height: height);
  }

}