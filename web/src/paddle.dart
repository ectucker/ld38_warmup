part of game;

class Paddle {

  Texture texture;
  double x, y;
  double width, height;

  Aabb2 hitbox;

  Paddle(this.texture, this.x, this.y, this.width, this.height) {
    hitbox = new Aabb2.centerAndHalfExtents(new Vector2(x - width / 2 + width, y - height / 2 + height), new Vector2(width / 2, height / 2));
  }

  void move(double amount) {
    x += amount;
    hitbox.setCenterAndHalfExtents(new Vector2(x - width / 2 + width, y - height / 2 + height), new Vector2(width / 2, height / 2));
  }

  void render(SpriteBatch batch) {
    batch.draw(texture, x, y, width: width, height: height);
  }

}