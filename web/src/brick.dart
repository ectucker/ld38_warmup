part of game;

class Brick {

  Texture texture;
  double x, y;
  double width, height;

  Aabb2 hitbox;

  Brick(this.texture, this.x, this.y, this.width, this.height) {
    hitbox = new Aabb2.centerAndHalfExtents(new Vector2(x - width / 2 + width, y - height / 2 + height), new Vector2(width / 2, height / 2));
  }

  void render(SpriteBatch batch) {
    batch.draw(texture, x + 1, y + 1, width: width - 2, height: height - 2);
  }

}