// Copyright (c) 2017, Ethan Tucker. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library game;

import 'package:cobblestone/cobblestone.dart';

part 'game.dart';
part 'brick.dart';
part 'paddle.dart';
part 'ball.dart';

void main() {
  new JuicyBreakout();
}
